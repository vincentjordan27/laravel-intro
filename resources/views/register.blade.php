<!DOCTYPE html>
<html>
    <head>
        <title>Form Sign Up</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
            <input type="hidden" name="_token" value = "<?php echo csrf_token() ?>">
            <label for="firstName">First name:</label>
            <br><br>
            <input type="text" id="firstName" name="firstName"> 
            <br><br>
            <label for="lastName">Last name:</label>
            <br><br>
            <input type="text" id="lastName" name="lastName">
            <br><br>

            <label>Gender:</label>
            <br><br>
            <input type="radio" name="gender" value="0">Male
            <br>
            <input type="radio" name="gender" value="1">Female
            <br>
            <input type="radio" name="gender" value="2">Other
            <br><br>

            <label>Nationality</label>
            <br><br>
            <select>
                <option value="indonesia">Indonesian</option>
                <option value="singaporean">Singaporean</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
            </select>
            <br><br>
            
            <label>Language Spoken:</label>
            <br><br>
            <input type="checkbox" name="language" value="0">Bahasa Indonesia
            <br>
            <input type="checkbox" name="language" value="1">English
            <br>
            <input type="checkbox" name="language" value="2">Other
            <br><br>

            <label>Bio:</label>
            <br><br>
            <textarea cols="30" rows="10"></textarea>
            <br>

            <input type="submit" value="Sign Up" >
        </form>
    </body>
</html>
